/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg485;
import java.util.*;
import java.math.BigInteger;

/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static BigInteger prev[] = new BigInteger[10000];
    public static BigInteger present[] = new BigInteger[10000];

    public static void main(String[] args) {
        // TODO code application logic here
        boolean flag = false;
        BigInteger tenPower60 = BigInteger.valueOf(10).pow(60);
        System.out.println("1");
        for (int n = 1; ; ++n) {
             present[0] = BigInteger.valueOf(1);
             System.out.print(present[0]);
             for (int k = 1; k < n; ++k) {
                 present[k] = prev[k - 1].add(prev[k]);
                 System.out.print(" "+ present[k]);
                 if (present[k].compareTo(tenPower60) > -1) {
                     flag = true;
                 }
             }
             present[n] = BigInteger.valueOf(1);
             System.out.println(" 1");
             for (int i = 0; i <= n; ++i) {
                 prev[i] = present[i];
             }
            if (flag == true) break;

        }
    }
}
