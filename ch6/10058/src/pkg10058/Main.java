/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10058;
import java.util.regex.*;
import java.util.Scanner;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static String Article = "(a|the)";
    public static String Noun = 
            "(tom|jerry|goofy|mickey|jimmy|dog|cat|mouse)";
    public static String Verb = "(hate|love|know|like)s*";
    public static String Actor = "(" + Article + " )?" + Noun; 
    public static String ActiveList = "(" + Actor + " and )*" + Actor;   
//    public static String ActiveList = Actor + "( and " + Actor  + ")*";

    public static String Action = ActiveList + " " + Verb + " " + ActiveList;
    public static String Statement = Action + "( , " + Action + ")*";
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine() == true) {
            String inputStr =  sc.nextLine();
            if (inputStr.matches(Statement)) {
                System.out.println("YES I WILL");
            } else {
                System.out.println("NO I WON'T");
            }
        }
    }
    
    
}
