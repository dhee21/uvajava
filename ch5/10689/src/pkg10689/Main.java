/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10689;
import java.util.Scanner;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static BigInteger fib[] = new BigInteger[15000];
    public static BigInteger get (long n) {
        return BigInteger.valueOf(n);
    }
    public static void main(String[] args) {
        // TODO code application logic here
        fib[0] = new BigInteger("0"); fib[1] = new BigInteger("1");
        for (int i = 2 ; i < 15000; ++i) {
            fib[i] = fib[i - 1].add(fib[i - 2]);
        }
        Scanner sc = new Scanner (System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            BigInteger a, b, n; int m;
            a = sc.nextBigInteger(); b = sc.nextBigInteger(); n = sc.nextBigInteger();
            m = sc.nextInt();
            BigInteger M = BigInteger.TEN.pow(m);
            if (n == BigInteger.ZERO) {
                System.out.println(a.mod(M));
                continue;
            }
            BigInteger one = get(1);
            BigInteger val1 = findFibMod(n.subtract(one), m);
            BigInteger val2 = findFibMod(n, m);
            BigInteger ans1 = (a.mod(M).multiply(val1)).mod(M);
            BigInteger ans2 = (b.mod(M).multiply(val2)).mod(M);
            System.out.println(ans1.add(ans2).mod(M));
           
        }
        
    }
    public static BigInteger findFibMod(BigInteger n, int m) {
        long PiVal[] = {60, 300, 1500, 15000};
        BigInteger rem = n.mod(BigInteger.valueOf(PiVal[m - 1]));
        return fib[rem.intValue()].mod(BigInteger.TEN.pow(m));
    }
}
