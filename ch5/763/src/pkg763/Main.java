/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg763;
import java.util.Scanner;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static BigInteger fib[] = new BigInteger[105];
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        fib[1] = new BigInteger("1"); fib[2] = new BigInteger("2");
        for (int i = 3; i < 103; ++i) {
            fib[i] = fib[i - 1].add(fib[i - 2]);
        }
        int caseNo = 0;
        while(sc.hasNext()) {
            if (caseNo++ > 0) System.out.println();
            String first = sc.next(), second = sc.next();
            BigInteger firstVal = giveNumber(first);
            BigInteger secondVal = giveNumber(second);
            BigInteger sum = firstVal.add(secondVal);
            String ans = giveFibonacci(sum);
            System.out.println(ans);
        } 
    }
    public static BigInteger giveNumber (String number) {
        int length = number.length();
        BigInteger value = BigInteger.ZERO;
        for (int i = 0; i < length; ++i) {
            int fibIndex = length - i;
            if (number.charAt(i) == '1') {
                value = value.add(fib[fibIndex]);
            }
        }
        return value;
    }
    public static String giveFibonacci (BigInteger number) {
        int i;
        for (i = 102; i >= 1; --i) if (fib[i].compareTo(number) < 1) break;
        String ans = "";
        for (int j = i; j >= 1; --j) {
            if (fib[j].compareTo(number) < 1 ) {
                number = number.subtract(fib[j]);
                ans += "1";
            } else {
                ans += "0";
            }
        }
        if (ans.length() == 0) ans = "0";
        return ans;
    }
}
