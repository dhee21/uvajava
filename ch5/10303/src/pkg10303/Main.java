/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10303;
import java.util.*;
import java.math.*;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static int lastVal = 1000;

    public static BigInteger catalan[] = new BigInteger[lastVal + 10];
    public static BigInteger get (long i) {
        return BigInteger.valueOf(i);
    }
    public static void Catalan () {
        catalan[0] = get(1);
        for (int n = 0 ; n < lastVal; ++n) {
            BigInteger two = get(2);
            BigInteger twoNPlusOne = get(1).add(two.multiply(get(n))) ;
            catalan[n + 1] = (twoNPlusOne.multiply(two).multiply(catalan[n])).
                    divide(get(n).add(two));
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        Catalan();
        while (sc.hasNextInt()) {
            int inp = sc.nextInt();
            if (inp == 0) break;
            System.out.println(catalan[inp]);
        }
    }
    
}

