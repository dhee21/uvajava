/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11879;
import java.util.Scanner;
import java.math.BigInteger;

/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        BigInteger seventeen = BigInteger.valueOf(17);

        while (true) {
            String a = sc.next();
            if (a.length() == 1 && a.charAt(0) == '0') break;
            if (a.length() == 1) {
                System.out.println("0"); continue;
            }
            String last = a.substring(a.length() - 1, a.length());
            String rest = a.substring(0, a.length() - 1);
            BigInteger lastInt = new BigInteger(last);
            BigInteger restInt = new BigInteger (rest);
            restInt = restInt.subtract(lastInt.multiply(BigInteger.valueOf(5)));
            System.out.println(restInt.mod(seventeen) == BigInteger.ZERO ? 1 : 0);
        }
    }
    
}
