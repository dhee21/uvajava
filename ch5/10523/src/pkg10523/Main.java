/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10523;
import java.util.Scanner;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int N, A;
        Scanner sc = new Scanner (System.in);
        while (sc.hasNextInt()) {
            N = sc.nextInt(); A = sc.nextInt();
            if (A == 0) {
                System.out.println("0"); continue;
            }
            BigInteger powerOfA = BigInteger.valueOf(A);
            BigInteger ans = BigInteger.ZERO;
            for (int i = 1; i <= N; ++i) {
                ans = ans.add(BigInteger.valueOf(i).multiply(powerOfA));
                powerOfA = powerOfA.multiply(BigInteger.valueOf(A));
            }
            System.out.println(ans);
        }
    }
    
}
