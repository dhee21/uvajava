/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11821;
import java.util.Scanner;
import java.math.BigDecimal;

/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            BigDecimal ans = BigDecimal.ZERO;
            while (true) {
                BigDecimal inp = sc.nextBigDecimal();
                String inpStr = inp.toString();
                if (inpStr.length() == 1 && inpStr.charAt(0) == '0') break;
                ans = ans.add(inp);
            }
            String ansStr = ans.stripTrailingZeros().toPlainString();
            System.out.println(ansStr);
        }
        
    }
    
}
