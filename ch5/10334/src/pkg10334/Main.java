/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10334;
import java.util.Scanner;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {
    /**
     * @param args the command line arguments
     */
    public static BigInteger fib[] = new BigInteger[1002];
    public static void main(String[] args) {
        // TODO code application logic here
        fib[0] = new BigInteger("1"); fib[1] = new BigInteger("2");
        for (int i = 2; i <= 1000; ++i) fib[i] = fib[i - 1].add(fib[i - 2]);
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextInt()) {
            int n = sc.nextInt();
            System.out.println(fib[n]);
        }
    }
    
}
