/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10925;
import java.util.Scanner;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int caseNo = 0;
        while (true) {
           caseNo ++;
           int n, f;
           n = sc.nextInt(); f = sc.nextInt();
           if (n == 0 && f == 0) break;
           BigInteger sum = BigInteger.ZERO;
           int toDivide = f;
           while (n-- > 0) {
               BigInteger billVal =sc.nextBigInteger();
               sum = sum.add(billVal);
           }
           System.out.println("Bill #" + caseNo  +  " costs " + sum 
                   + ": each friend should pay " + 
                   sum.divide(BigInteger.valueOf(toDivide)) + "\n");
        }
    }
    
}
