/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg389;
import java.util.Scanner;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String numStr = sc.next();
            int a = sc.nextInt(), b = sc.nextInt();
            Integer num = Integer.parseInt(numStr, a);
            String converted = Integer.toString(num, b);
            String ans = "";
            if (converted.length() > 7) {
                ans = "ERROR";
            } else {
                ans = converted;
            }
            
            int spaces = 7 - ans.length();
            String toAddFront = "";
            for (int i = 0; i < spaces; ++i) toAddFront += " ";
            System.out.println(toAddFront + ans.toUpperCase());
        }
    }
    
}
