/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10007;
import java.util.*;
import java.math.*;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static BigInteger catalan[] = new BigInteger[310];
    public static BigInteger trees[] = new BigInteger[310];
    public static BigInteger get (long i) {
        return BigInteger.valueOf(i);
    }
    public static void Catalan () {
        catalan[0] = get(1);
        for (int n = 0 ; n < 300; ++n) {
            BigInteger two = get(2);
            BigInteger twoNPlusOne = get(1).add(two.multiply(get(n))) ;
            catalan[n + 1] = (twoNPlusOne.multiply(two).multiply(catalan[n])).
                    divide(get(n).add(two));
        }
    }
    public static void trees () {
        BigInteger fact = get(1);
        trees[0] = catalan[0];
        for (int i = 1; i <= 300; ++i) {
            trees[i] = fact.multiply(catalan[i]);
            fact = fact.multiply(get(i + 1));
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        Catalan();
        trees();
        while (true) {
            int inp = sc.nextInt();
            if (inp == 0) break;
            System.out.println(trees[inp]);
        }
    }
    
}
