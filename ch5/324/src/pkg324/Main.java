/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg324;
import java.util.Scanner;
import java.util.Arrays;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {
    
    public static BigInteger get (long n) {
        return BigInteger.valueOf(n);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int input = sc.nextInt();
            if (input == 0) break;
            BigInteger n = get(1);
            for (int i = 1; i <= input ; ++i) {
                n = n.multiply(get(i));
            }
            String s = n.toString();
            int freq[] = new int[10];
            Arrays.fill(freq, 0);
            for (int i = 0; i < s.length() ; ++i ) {
                freq[s.charAt(i) - '0'] ++;
            }
            System.out.println(input + "! --");
            System.out.print("   (" + 0 +  ")");
            System.out.print(rightJustify(freq[0]));
            for (int i = 1; i < 5; ++i) {
                System.out.print("    (" + i +  ")");
                System.out.print(rightJustify(freq[i]));
            }
            System.out.print("\n");
            System.out.print("   (" + 5 +  ")");
            System.out.print(rightJustify(freq[5]));
            for (int i = 6; i < 10; ++i) {
                System.out.print("    (" + i +  ")");
                System.out.print(rightJustify(freq[i]));
            }
            System.out.print("\n");
        }
        
    }
    public static String rightJustify (int i) {
        String s = Integer.toString(i);
        while (s.length() < 5) {
            s = " " + s;
        }
        return s;
    }
    
}
