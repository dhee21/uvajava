/* package whatever; // don't place package name! */
package pkg10814;
import java.util.Scanner;
import java.math.BigInteger;

/* Name of the class has to be "Main" only if the class is public. */
class Main
{
	public static void main (String[] args)
	{
		// your code goes here
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while (t-- > 0) {
			BigInteger a = sc.nextBigInteger();
			sc.next();
			BigInteger b = sc.nextBigInteger();
			BigInteger gcdVal = a.gcd(b);
			System.out.println(a.divide(gcdVal) + " / " + b.divide(gcdVal) );
		}
	}
}