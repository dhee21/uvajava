/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10219;
import java.util.*;
import java.math.*;
/**
 *
 * @author Dheeraj
 */
public class Main {
    
    public static BigInteger get (long x) {
        return BigInteger.valueOf(x);
    }
        
    public static BigInteger NCR (BigInteger n, BigInteger k) {
        if (n.compareTo(get(0)) == -1) return get(0);
        if (n.compareTo(get(0)) == 0) return get(1);
        BigInteger nMinusK = n.subtract(k);
        if (k.compareTo(nMinusK) > 0) {
            k = nMinusK;
        }

        if (k.compareTo(get(0)) == 0) return get(1);
        BigInteger ans = n;
        BigInteger one = get(1);
        for (BigInteger i = get(2); i.compareTo(k) <= 0; i = i.add(one)) {
            n = n.subtract(one);
            ans = ans.multiply(n).divide(i);
        }
        return ans;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        while (sc.hasNextBigInteger()) {
            BigInteger n = sc.nextBigInteger(), r = sc.nextBigInteger();
            BigInteger val = NCR(n, r);
//            System.out.println(val);
            System.out.println(val.toString().length());
        }
    }
    
}
