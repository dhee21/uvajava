/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10551;
import java.util.Scanner;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int b = sc.nextInt();
            if ( b == 0 ) break;
            String pStr, mStr;
            pStr = sc.next(); mStr = sc.next();
            BigInteger p = new BigInteger(pStr, b), m = new BigInteger(mStr, b);
            System.out.println(p.mod(m).toString(b));
            
        }
    }
    
}
