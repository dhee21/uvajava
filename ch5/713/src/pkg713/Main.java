/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg713;
import java.util.Scanner;
import java.math.BigInteger;
/**
 *
 * @author Dheeraj
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        String rev1, rev2;
        int n;
        n = sc.nextInt();
        for (int i = 0; i < n ; ++i) {
        	rev1 = sc.next(); rev2 = sc.next();
        	String oneStr , twoStr;
                oneStr = reverse(rev1);
                twoStr = reverse(rev2);
        	BigInteger one = new BigInteger(oneStr);
                BigInteger two = new BigInteger(twoStr);
                StringBuffer ans = reverseAns(one.add(two).toString());
        	System.out.println(ans);
        }
    }
    public static String reverse (String s) {
        String revString = "";
        for (int j = 0 ; j < s.length(); ++j) {
                    revString = s.charAt(j) + revString;
         }
        return revString ;
    }
    public static StringBuffer reverseAns(String s) {
        StringBuffer str = new StringBuffer (s);
        str.reverse();
        while (str.charAt(0) == '0') {
            str.delete(0, 1);
        }
        return str;
    }
}
