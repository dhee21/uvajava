/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg623;
import java.util.*;
import java.math.*;
/**
 *
 * @author Dheeraj
 */
public class Main {
    public static BigInteger get (long i) {
        return BigInteger.valueOf(i);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        while (sc.hasNextInt()) {
            int input = sc.nextInt();
            BigInteger ans = get(1);
            for (int i = 1; i <= input ; ++i) {
                ans = ans.multiply(get(i));
            }
            System.out.println(input + "!");
            System.out.println(ans);
        }
    }
    
}
